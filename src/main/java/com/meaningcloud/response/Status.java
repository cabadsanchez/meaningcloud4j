package com.meaningcloud.response;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Status {

	private int code;
	private String msg;
	private int credits;
	@JsonProperty("remaining_credits")
	private int remainingCredits;
	
	public Status() {}

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public int getCredits() {
		return credits;
	}

	public void setCredits(int credits) {
		this.credits = credits;
	}

	public int getRemainingCredits() {
		return remainingCredits;
	}

	public void setRemainingCredits(int remainingCcredits) {
		this.remainingCredits = remainingCcredits;
	}
}
