package com.meaningcloud.response;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.meaningcloud.domain.sentiment.Sentence;
import com.meaningcloud.domain.sentiment.SentimentedConcept;
import com.meaningcloud.domain.sentiment.SentimentedEntity;

public class Sentiment {

	public Status status;
	public String model;
	@JsonProperty("score_tag")
	public String scoreTag;
	public String agreement;
	public String subjectivity;
	public int confidence;
	public String irony;
	@JsonProperty("sentence_list")
	public List<Sentence> sentences;
	@JsonProperty("sentimented_entity_list")
	public List<SentimentedEntity> sentimentedEntities;
	@JsonProperty("sentimented_concept_list")
	public List<SentimentedConcept> sentimentedConcepts;
	
	public Sentiment() {}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public String getScoreTag() {
		return scoreTag;
	}

	public void setScoreTag(String scoreTag) {
		this.scoreTag = scoreTag;
	}

	public String getAgreement() {
		return agreement;
	}

	public void setAgreement(String agreement) {
		this.agreement = agreement;
	}

	public String getSubjectivity() {
		return subjectivity;
	}

	public void setSubjectivity(String subjectivity) {
		this.subjectivity = subjectivity;
	}

	public int getConfidence() {
		return confidence;
	}

	public void setConfidence(int confidence) {
		this.confidence = confidence;
	}

	public String getIrony() {
		return irony;
	}

	public void setIrony(String irony) {
		this.irony = irony;
	}

	public List<Sentence> getSentences() {
		return sentences;
	}

	public void setSentences(List<Sentence> sentences) {
		this.sentences = sentences;
	}

	public List<SentimentedEntity> getSentimentedEntities() {
		return sentimentedEntities;
	}

	public void setSentimentedEntities(List<SentimentedEntity> sentimentedEntities) {
		this.sentimentedEntities = sentimentedEntities;
	}

	public List<SentimentedConcept> getSentimentedConcepts() {
		return sentimentedConcepts;
	}

	public void setSentimentedConcepts(List<SentimentedConcept> sentimentedConcepts) {
		this.sentimentedConcepts = sentimentedConcepts;
	}
}
