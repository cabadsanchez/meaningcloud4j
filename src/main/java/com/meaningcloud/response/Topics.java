package com.meaningcloud.response;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.meaningcloud.domain.topics.Concept;
import com.meaningcloud.domain.topics.Entity;
import com.meaningcloud.domain.topics.MoneyExpression;
import com.meaningcloud.domain.topics.OtherExpression;
import com.meaningcloud.domain.topics.PhoneExpression;
import com.meaningcloud.domain.topics.Quotation;
import com.meaningcloud.domain.topics.Relation;
import com.meaningcloud.domain.topics.TimeExpression;
import com.meaningcloud.domain.topics.Uri;

public class Topics {

	public Status status;
	@JsonProperty("entity_list")
	public List<Entity> entities;
	@JsonProperty("concept_list")
	public List<Concept> concepts;
	@JsonProperty("time_expression_list")
	public List<TimeExpression> timeExpressions;
	@JsonProperty("money_expression_list")
	public List<MoneyExpression> moneyExpressions;
	@JsonProperty("uri_list")
	public List<Uri> uris;
	@JsonProperty("phone_expression_list")
	public List<PhoneExpression> phoneExpressions;
	@JsonProperty("other_expression_list")
	public List<OtherExpression> otherExpressions;
	@JsonProperty("quotation_list")
	public List<Quotation> quotations;
	@JsonProperty("relation_list")
	public List<Relation> relations;
	
	public Topics() {}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public List<Entity> getEntities() {
		return entities;
	}

	public void setEntities(List<Entity> entities) {
		this.entities = entities;
	}

	public List<Concept> getConcepts() {
		return concepts;
	}

	public void setConcepts(List<Concept> concepts) {
		this.concepts = concepts;
	}

	public List<TimeExpression> getTimeExpressions() {
		return timeExpressions;
	}

	public void setTimeExpressions(List<TimeExpression> timeExpressions) {
		this.timeExpressions = timeExpressions;
	}

	public List<MoneyExpression> getMoneyExpressions() {
		return moneyExpressions;
	}

	public void setMoneyExpressions(List<MoneyExpression> moneyExpressions) {
		this.moneyExpressions = moneyExpressions;
	}

	public List<Uri> getUris() {
		return uris;
	}

	public void setUris(List<Uri> uris) {
		this.uris = uris;
	}

	public List<PhoneExpression> getPhoneExpressions() {
		return phoneExpressions;
	}

	public void setPhoneExpressions(List<PhoneExpression> phoneExpressions) {
		this.phoneExpressions = phoneExpressions;
	}

	public List<OtherExpression> getOtherExpressions() {
		return otherExpressions;
	}

	public void setOtherExpressions(List<OtherExpression> otherExpressions) {
		this.otherExpressions = otherExpressions;
	}

	public List<Quotation> getQuotations() {
		return quotations;
	}

	public void setQuotations(List<Quotation> quotations) {
		this.quotations = quotations;
	}

	public List<Relation> getRelations() {
		return relations;
	}

	public void setRelations(List<Relation> relations) {
		this.relations = relations;
	}
}
