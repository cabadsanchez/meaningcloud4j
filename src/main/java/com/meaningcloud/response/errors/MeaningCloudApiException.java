package com.meaningcloud.response.errors;

public class MeaningCloudApiException extends Throwable {

	private static final long serialVersionUID = -270617089862616639L;

	private final int code;
	private final String message;
	
	public MeaningCloudApiException(int code, String message) {
		super("API error " + Integer.toString(code) + ": " +  message);
		this.code = code;
		this.message = message;
	}

	public int getCode() {
		return code;
	}

	public String getMessage() {
		return message;
	}
}
