package com.meaningcloud.response.errors;

public class MeaningCloudException extends Throwable {

	private static final long serialVersionUID = -2473282208458608590L;
	
	private final int httpCode;
	private final String httpMessage;
	
	public MeaningCloudException(int httpCode, String httpMessage) {
		super("Server returned " + Integer.toString(httpCode) + ": " +  httpMessage);
    	this.httpCode = httpCode;
    	this.httpMessage = httpMessage;
	}

	public int getHttpCode() {
		return httpCode;
	}

	public String getHttpMessage() {
		return httpMessage;
	}
}
