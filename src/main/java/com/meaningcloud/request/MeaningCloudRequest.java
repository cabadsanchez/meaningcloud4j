package com.meaningcloud.request;

import java.util.Map;

import com.mashape.unirest.http.HttpMethod;
import com.mashape.unirest.request.HttpRequestWithBody;
import com.mashape.unirest.request.body.MultipartBody;

public class MeaningCloudRequest extends HttpRequestWithBody {
	
	protected static Map<String, Object> params;
	
	public MeaningCloudRequest(HttpMethod method, String url) {
		super(method, url);
		super.queryString("src", "meaningcloud4j");
	}

	public MeaningCloudRequest licenseKey(String key) {
		params.put("key", key);
		return this;
	}
	
	public MultipartBody build() {
		return this.fields(params);
	}
}