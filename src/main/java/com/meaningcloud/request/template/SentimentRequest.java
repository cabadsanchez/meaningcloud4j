package com.meaningcloud.request.template;

import java.util.HashMap;

import com.mashape.unirest.http.HttpMethod;
import com.meaningcloud.request.MeaningCloudRequest;

public class SentimentRequest extends MeaningCloudRequest {
	
	public SentimentRequest(HttpMethod method, String url) {
		super(method, url);
		params = new HashMap<String, Object>();
	}

	public SentimentRequest outputFormat(String of) {
		params.put("of", of);
		return this;
	}
	
	public SentimentRequest verbose(String verbose) {
		params.put("verbose", verbose);
		return this;
	}
	
	public SentimentRequest text(String txt) {
		params.put("txt", txt);
		return this;
	}
	
	public SentimentRequest textFormat(String txtf) {
		params.put("txtf", txtf);
		return this;
	}
	
	public SentimentRequest model(String model) {
		params.put("model", model);
		return this;
	}
	
	public SentimentRequest treeView(String tv) {
		params.put("tv", tv);
		return this;
	}
	
	public SentimentRequest expandGlobalPolarity(String egp) {
		params.put("egp", egp);
		return this;
	}
	
	public SentimentRequest topicTypes(String tt) {
		params.put("tt", tt);
		return this;
	}
	
	public SentimentRequest relaxedTipography(String rt) {
		params.put("rt", rt);
		return this;
	}
	
	public SentimentRequest unknownWords(String uw) {
		params.put("uw", uw);
		return this;
	}
	
	public SentimentRequest disambiguationLevel(String dm) {
		params.put("dm", dm);
		return this;
	}
	
	public SentimentRequest semanticDisambiguationGrouping(String sdg) {
		params.put("sdg", sdg);
		return this;
	}
	
	public SentimentRequest disambiguationContext(String cont) {
		params.put("cont", cont);
		return this;
	}
	
	public SentimentRequest showSubtopics(String st) {
		params.put("st", st);
		return this;
	}
	
	public SentimentRequest caseSensitiveConcepts(String cs) {
		params.put("cs", cs);
		return this;
	}
	
	public SentimentRequest timeContext(String timeref) {
		params.put("timeref", timeref);
		return this;
	}
	
	public SentimentRequest userDictionary(String ud) {
		params.put("ud", ud);
		return this;
	}
}
