package com.meaningcloud.request.template;

import java.io.File;
import java.util.HashMap;

import com.mashape.unirest.http.HttpMethod;
import com.meaningcloud.request.MeaningCloudRequest;

public class TopicsRequest extends MeaningCloudRequest {

	public TopicsRequest(HttpMethod method, String url) {
		super(method, url);
		params = new HashMap<String, Object>();
	}

	public TopicsRequest outputFormat(String of) {
		params.put("of", of);
		return this;
	}
	
	public TopicsRequest language(String lang) {
		params.put("lang", lang);
		return this;
	}
	
	public TopicsRequest text(String txt) {
		params.put("txt", txt);
		return this;
	}
	
	public TopicsRequest textFormat(String txtf) {
		params.put("txtf", txtf);
		return this;
	}
	
	public TopicsRequest url(String url) {
		params.put("url", url);
		return this;
	}
	
	public TopicsRequest document(String doc) {
		params.put("doc", new File(doc));
		return this;
	}
	
	public TopicsRequest topicTypes(String tt) {
		params.put("tt", tt);
		return this;
	}
	
	public TopicsRequest unknownWords(String uw) {
		params.put("uw", uw);
		return this;
	}
	
	public TopicsRequest relaxedTipography(String rt) {
		params.put("rt", rt);
		return this;
	}
	
	public TopicsRequest dictionary(String dic) {
		params.put("dic", dic);
		return this;
	}
	
	public TopicsRequest userDictionary(String ud) {
		params.put("ud", ud);
		return this;
	}
	
	public TopicsRequest showSubtopics(String st) {
		params.put("st", st);
		return this;
	}
}
