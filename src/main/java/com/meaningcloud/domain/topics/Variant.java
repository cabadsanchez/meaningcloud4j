package com.meaningcloud.domain.topics;

public class Variant {

	private String form;
	private int inip;
	private int endp;
	
	public Variant() {}

	public String getForm() {
		return form;
	}

	public void setForm(String form) {
		this.form = form;
	}

	public int getInip() {
		return inip;
	}

	public void setInip(int inip) {
		this.inip = inip;
	}

	public int getEndp() {
		return endp;
	}

	public void setEndp(int endp) {
		this.endp = endp;
	}
}
