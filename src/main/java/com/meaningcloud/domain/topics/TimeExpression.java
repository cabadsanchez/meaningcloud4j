package com.meaningcloud.domain.topics;

import com.fasterxml.jackson.annotation.JsonProperty;

public class TimeExpression {

	private String form;
	@JsonProperty("normalized_form")
	private String normalizedForm;
	@JsonProperty("actual_time")
	private String actualTime;
	private String precision;
	private int inip;
	private int endp;
	
	public TimeExpression() {}

	public String getForm() {
		return form;
	}

	public void setForm(String form) {
		this.form = form;
	}

	public String getNormalizedForm() {
		return normalizedForm;
	}

	public void setNormalizedForm(String normalizedForm) {
		this.normalizedForm = normalizedForm;
	}

	public String getActualTime() {
		return actualTime;
	}

	public void setActualTime(String actualTime) {
		this.actualTime = actualTime;
	}

	public String getPrecision() {
		return precision;
	}

	public void setPrecision(String precision) {
		this.precision = precision;
	}

	public int getInip() {
		return inip;
	}

	public void setInip(int inip) {
		this.inip = inip;
	}

	public int getEndp() {
		return endp;
	}

	public void setEndp(int endp) {
		this.endp = endp;
	}
}
