package com.meaningcloud.domain.topics;

import com.fasterxml.jackson.annotation.JsonProperty;

public class MoneyExpression {

	private String form;
	private String amount;
	@JsonProperty("numeric_value")
	private double numericValue;
	private String currency;
	private int inip;
	private int endp;
	
	public MoneyExpression() {}

	public String getForm() {
		return form;
	}

	public void setForm(String form) {
		this.form = form;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public double getNumericValue() {
		return numericValue;
	}

	public void setNumericValue(double numericValue) {
		this.numericValue = numericValue;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public int getInip() {
		return inip;
	}

	public void setInip(int inip) {
		this.inip = inip;
	}

	public int getEndp() {
		return endp;
	}

	public void setEndp(int endp) {
		this.endp = endp;
	}
}
