package com.meaningcloud.domain.topics;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Quotation {

	public String form;
	public String who;
	@JsonProperty("who_lemma")
	public String whoLemma;
	public String verb;
	@JsonProperty("verb_lemma")
	public String verbLemma;
	public int inip;
	public int endp;
	
	public Quotation() {}

	public String getForm() {
		return form;
	}

	public void setForm(String form) {
		this.form = form;
	}

	public String getWho() {
		return who;
	}

	public void setWho(String who) {
		this.who = who;
	}

	public String getWhoLemma() {
		return whoLemma;
	}

	public void setWhoLemma(String whoLemma) {
		this.whoLemma = whoLemma;
	}

	public String getVerb() {
		return verb;
	}

	public void setVerb(String verb) {
		this.verb = verb;
	}

	public String getVerbLemma() {
		return verbLemma;
	}

	public void setVerbLemma(String verbLemma) {
		this.verbLemma = verbLemma;
	}

	public int getInip() {
		return inip;
	}

	public void setInip(int inip) {
		this.inip = inip;
	}

	public int getEndp() {
		return endp;
	}

	public void setEndp(int endp) {
		this.endp = endp;
	}
}
