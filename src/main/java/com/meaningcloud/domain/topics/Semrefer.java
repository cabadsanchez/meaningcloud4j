package com.meaningcloud.domain.topics;

@SuppressWarnings("unused")
public class Semrefer {

	private Organization organization;
	private Affinity affinity;
	
	public Semrefer() {}

	public Organization getOrganization() {
		return organization;
	}

	public void setOrganization(Organization organization) {
		this.organization = organization;
	}

	public Affinity getAffinity() {
		return affinity;
	}

	public void setAffinity(Affinity affinity) {
		this.affinity = affinity;
	}
	
	private static class Organization {
		
		private String id;
		private String form;
		
		public Organization() {}

		public String getId() {
			return id;
		}

		public void setId(String id) {
			this.id = id;
		}

		public String getForm() {
			return form;
		}

		public void setForm(String form) {
			this.form = form;
		}
	}
	
	private static class Affinity {
		
		private String id;
		private String form;
		
		public Affinity() {}

		public String getId() {
			return id;
		}

		public void setId(String id) {
			this.id = id;
		}

		public String getForm() {
			return form;
		}

		public void setForm(String form) {
			this.form = form;
		}
	}
}
