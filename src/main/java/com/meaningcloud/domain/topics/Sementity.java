package com.meaningcloud.domain.topics;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Sementity {

	@JsonProperty("class")
	private String cl;
	private String fiction;
	private String id;
	private String type;
	private String confidence;
	
	public Sementity() {}

	public String getSemClass() {
		return cl;
	}

	public void setSemClass(String cl) {
		this.cl = cl;
	}

	public String getFiction() {
		return fiction;
	}

	public void setFiction(String fiction) {
		this.fiction = fiction;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getConfidence() {
		return confidence;
	}

	public void setConfidence(String confidence) {
		this.confidence = confidence;
	}
}
