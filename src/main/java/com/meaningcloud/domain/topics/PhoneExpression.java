package com.meaningcloud.domain.topics;

public class PhoneExpression {

	public String form;
	public int inip;
	public int endp;
	
	public PhoneExpression() {}

	public String getForm() {
		return form;
	}

	public void setForm(String form) {
		this.form = form;
	}

	public int getInip() {
		return inip;
	}

	public void setInip(int inip) {
		this.inip = inip;
	}

	public int getEndp() {
		return endp;
	}

	public void setEndp(int endp) {
		this.endp = endp;
	}
}
