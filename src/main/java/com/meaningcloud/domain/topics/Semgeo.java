package com.meaningcloud.domain.topics;

import com.fasterxml.jackson.annotation.JsonProperty;

@SuppressWarnings("unused")
public class Semgeo {

	private Geoentity continent;
	private Geoentity country;
	private Geoentity adm1;
	private Geoentity adm2;
	private Geoentity adm3;
	private Geoentity city;
	private Geoentity district;
	
	public Semgeo() {}

	public Geoentity getContinent() {
		return continent;
	}

	public void setContinent(Geoentity continent) {
		this.continent = continent;
	}

	public Geoentity getCountry() {
		return country;
	}

	public void setCountry(Geoentity country) {
		this.country = country;
	}

	public Geoentity getAdm1() {
		return adm1;
	}

	public void setAdm1(Geoentity adm1) {
		this.adm1 = adm1;
	}

	public Geoentity getAdm2() {
		return adm2;
	}

	public void setAdm2(Geoentity adm2) {
		this.adm2 = adm2;
	}

	public Geoentity getAdm3() {
		return adm3;
	}

	public void setAdm3(Geoentity adm3) {
		this.adm3 = adm3;
	}

	public Geoentity getCity() {
		return city;
	}

	public void setCity(Geoentity city) {
		this.city = city;
	}

	public Geoentity getDistrict() {
		return district;
	}

	public void setDistrict(Geoentity district) {
		this.district = district;
	}
	
	private static class Geoentity {
		private String form;
		private String id;
		@JsonProperty("ISO3166-1-a2")
		private String iso31661a2;
		@JsonProperty("ISO3166-1-a3")
		private String iso21661a3;
		@JsonProperty("BCBA")
		private String bcba;
		@JsonProperty("BCS")
		private String bcs;
		@JsonProperty("BMAD")
		private String bmad;
		@JsonProperty("BVL")
		private String bvl;
		@JsonProperty("Euronext")
		private String euronext;
		@JsonProperty("LSE")
		private String lse;
		@JsonProperty("LuxSE")
		private String luxse;
		@JsonProperty("NASDAQ")
		private String nasdaq;
		@JsonProperty("NYSE")
		private String nyse;
		@JsonProperty("ISO4217")
		private String iso4217;
		@JsonProperty("ISO639-1")
		private String iso6391;
		@JsonProperty("ISO639-2")
		private String iso6392;
		@JsonProperty("ISO639-3")
		private String iso6393;
		@JsonProperty("ISO639-5")
		private String iso6395;
		
		public Geoentity() {}

		public String getForm() {
			return form;
		}

		public void setForm(String form) {
			this.form = form;
		}

		public String getId() {
			return id;
		}

		public void setId(String id) {
			this.id = id;
		}

		public String getIso31661a2() {
			return iso31661a2;
		}

		public void setIso31661a2(String iso31661a2) {
			this.iso31661a2 = iso31661a2;
		}

		public String getIso21661a3() {
			return iso21661a3;
		}

		public void setIso21661a3(String iso21661a3) {
			this.iso21661a3 = iso21661a3;
		}

		public String getBcba() {
			return bcba;
		}

		public void setBcba(String bcba) {
			this.bcba = bcba;
		}

		public String getBcs() {
			return bcs;
		}

		public void setBcs(String bcs) {
			this.bcs = bcs;
		}

		public String getBmad() {
			return bmad;
		}

		public void setBmad(String bmad) {
			this.bmad = bmad;
		}

		public String getBvl() {
			return bvl;
		}

		public void setBvl(String bvl) {
			this.bvl = bvl;
		}

		public String getEuronext() {
			return euronext;
		}

		public void setEuronext(String euronext) {
			this.euronext = euronext;
		}

		public String getLse() {
			return lse;
		}

		public void setLse(String lse) {
			this.lse = lse;
		}

		public String getLuxse() {
			return luxse;
		}

		public void setLuxse(String luxse) {
			this.luxse = luxse;
		}

		public String getNasdaq() {
			return nasdaq;
		}

		public void setNasdaq(String nasdaq) {
			this.nasdaq = nasdaq;
		}

		public String getNyse() {
			return nyse;
		}

		public void setNyse(String nyse) {
			this.nyse = nyse;
		}

		public String getIso4217() {
			return iso4217;
		}

		public void setIso4217(String iso4217) {
			this.iso4217 = iso4217;
		}

		public String getIso6391() {
			return iso6391;
		}

		public void setIso6391(String iso6391) {
			this.iso6391 = iso6391;
		}

		public String getIso6392() {
			return iso6392;
		}

		public void setIso6392(String iso6392) {
			this.iso6392 = iso6392;
		}

		public String getIso6393() {
			return iso6393;
		}

		public void setIso6393(String iso6393) {
			this.iso6393 = iso6393;
		}

		public String getIso6395() {
			return iso6395;
		}

		public void setIso6395(String iso6395) {
			this.iso6395 = iso6395;
		}
	}
}
