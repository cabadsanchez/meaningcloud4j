package com.meaningcloud.domain.topics;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

@SuppressWarnings("unused")
public class Relation {

	public String form;
	public int inip;
	public int endp;
	public Subject subject;
	public Verb verb;
	@JsonProperty("complement_list")
	public List<Complement> complements;
	public int degree;
	
	public Relation() {}

	public String getForm() {
		return form;
	}

	public void setForm(String form) {
		this.form = form;
	}

	public int getInip() {
		return inip;
	}

	public void setInip(int inip) {
		this.inip = inip;
	}

	public int getEndp() {
		return endp;
	}

	public void setEndp(int endp) {
		this.endp = endp;
	}

	public Subject getSubject() {
		return subject;
	}

	public void setSubject(Subject subject) {
		this.subject = subject;
	}

	public Verb getVerb() {
		return verb;
	}

	public void setVerb(Verb verb) {
		this.verb = verb;
	}

	public List<Complement> getComplements() {
		return complements;
	}

	public void setComplements(List<Complement> complements) {
		this.complements = complements;
	}

	public int getDegree() {
		return degree;
	}

	public void setDegree(int degree) {
		this.degree = degree;
	}
	
	private static class Subject {
		
		private String form;
		@JsonProperty("lemma_list")
		private List<String> lemmas;
		@JsonProperty("sense_id_list")
		private List<String> senses;
		
		public Subject() {}

		public String getForm() {
			return form;
		}

		public void setForm(String form) {
			this.form = form;
		}

		public List<String> getLemmas() {
			return lemmas;
		}

		public void setLemmas(List<String> lemmas) {
			this.lemmas = lemmas;
		}

		public List<String> getSenses() {
			return senses;
		}

		public void setSenses(List<String> senses) {
			this.senses = senses;
		}
	}
	
	private static class Verb {
		
		public String form;
		@JsonProperty("lemma_list")
		public List<String> lemmas;
		@JsonProperty("sense_id_list")
		private List<String> senses;
		@JsonProperty("semantic_lemma_list")
		private List<String> semanticLemmas;
		
		public Verb() {}

		public String getForm() {
			return form;
		}

		public void setForm(String form) {
			this.form = form;
		}

		public List<String> getLemmas() {
			return lemmas;
		}

		public void setLemmas(List<String> lemmas) {
			this.lemmas = lemmas;
		}

		public List<String> getSenses() {
			return senses;
		}

		public void setSenses(List<String> senses) {
			this.senses = senses;
		}

		public List<String> getSemanticLemmas() {
			return semanticLemmas;
		}

		public void setSemanticLemmas(List<String> semanticLemmas) {
			this.semanticLemmas = semanticLemmas;
		}
	}
	
	private static class Complement {
		
		private String form;
		private String type;
		
		public Complement() {}

		public String getForm() {
			return form;
		}

		public void setForm(String form) {
			this.form = form;
		}

		public String getType() {
			return type;
		}

		public void setType(String type) {
			this.type = type;
		}
	}
}
