package com.meaningcloud.domain.topics;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Entity {

	private String form;
	private String dictionary;
	private String id;
	private Sementity sementity;
	@JsonProperty("semgeo_list")
	private List<Semgeo> semgeoList;
	@JsonProperty("semld_list")
	private List<String> semldList;
	@JsonProperty("semrefer_list")
	private List<Semrefer> semreferList;
	@JsonProperty("semtheme_list")
	private List<Semtheme> semthemeList;
	@JsonProperty("variant_list")
	private List<Variant> variantList;
	private double relevance;
	@JsonProperty("subentity_list")
	private List<Entity> subentityList;
	@JsonProperty("standard_list")
	private List<Standard> standardList;

	public String getForm() {
		return form;
	}
	public void setForm(String form) {
		this.form = form;
	}
	public String getDictionary() {
		return dictionary;
	}
	public void setDictionary(String dictionary) {
		this.dictionary = dictionary;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public Sementity getSementity() {
		return sementity;
	}
	public void setSementity(Sementity sementity) {
		this.sementity = sementity;
	}
	public List<Semgeo> getSemgeoList() {
		return semgeoList;
	}
	public void setSemgeoList(List<Semgeo> semgeoList) {
		this.semgeoList = semgeoList;
	}
	public List<String> getSemldList() {
		return semldList;
	}
	public void setSemldList(List<String> semldList) {
		this.semldList = semldList;
	}
	public List<Semrefer> getSemreferList() {
		return semreferList;
	}
	public void setSemreferList(List<Semrefer> semreferList) {
		this.semreferList = semreferList;
	}
	public List<Semtheme> getSemthemeList() {
		return semthemeList;
	}
	public void setSemthemeList(List<Semtheme> semthemeList) {
		this.semthemeList = semthemeList;
	}
	public List<Variant> getVariantList() {
		return variantList;
	}
	public void setVariantList(List<Variant> variantList) {
		this.variantList = variantList;
	}
	public double getRelevance() {
		return relevance;
	}
	public void setRelevance(double relevance) {
		this.relevance = relevance;
	}
	public List<Entity> getSubentityList() {
		return subentityList;
	}
	public void setSubentityList(List<Entity> subentityList) {
		this.subentityList = subentityList;
	}
	public List<Standard> getStandardList() {
		return standardList;
	}
	public void setStandardList(List<Standard> standardList) {
		this.standardList = standardList;
	}
}
