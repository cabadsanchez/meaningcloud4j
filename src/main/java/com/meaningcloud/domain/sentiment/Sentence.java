package com.meaningcloud.domain.sentiment;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Sentence {

	private String text;
	private int inip;
	private int endp;
	private String bop;
	private String confidence;
	@JsonProperty("score_tag")
	private String scoreTag;
	private String agreement;
	@JsonProperty("segment_list")
	private List<Segment> segments;
	@JsonProperty("sentimented_concept_list")
	private List<SentimentedConcept> sentimentedConcepts;
	@JsonProperty("sentimented_entity_list")
	private List<SentimentedEntity> sentimentedEntities;
	
	public Sentence() {}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public int getInip() {
		return inip;
	}

	public void setInip(int inip) {
		this.inip = inip;
	}

	public int getEndp() {
		return endp;
	}

	public void setEndp(int endp) {
		this.endp = endp;
	}

	public String getBop() {
		return bop;
	}

	public void setBop(String bop) {
		this.bop = bop;
	}

	public String getConfidence() {
		return confidence;
	}

	public void setConfidence(String confidence) {
		this.confidence = confidence;
	}

	public String getScoreTag() {
		return scoreTag;
	}

	public void setScoreTag(String scoreTag) {
		this.scoreTag = scoreTag;
	}

	public String getAgreement() {
		return agreement;
	}

	public void setAgreement(String agreement) {
		this.agreement = agreement;
	}

	public List<Segment> getSegments() {
		return segments;
	}

	public void setSegments(List<Segment> segments) {
		this.segments = segments;
	}

	public List<SentimentedConcept> getSentimentedConcepts() {
		return sentimentedConcepts;
	}

	public void setSentimentedConcepts(List<SentimentedConcept> sentimentedConcepts) {
		this.sentimentedConcepts = sentimentedConcepts;
	}

	public List<SentimentedEntity> getSentimentedEntities() {
		return sentimentedEntities;
	}

	public void setSentimentedEntities(List<SentimentedEntity> sentimentedEntities) {
		this.sentimentedEntities = sentimentedEntities;
	}
}
