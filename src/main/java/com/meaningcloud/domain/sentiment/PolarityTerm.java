package com.meaningcloud.domain.sentiment;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class PolarityTerm {

	private String text;
	private int inip;
	private int endp;
	@JsonProperty("tag_stack")
	private String tagStack;
	private String confidence;
	@JsonProperty("score_tag")
	private String scoreTag;
	@JsonProperty("sentimented_concept_list")
	private List<SentimentedConcept> sentimentedConcepts;
	@JsonProperty("sentimented_entity_list")
	private List<SentimentedEntity> sentimentedEntities;
	
	public PolarityTerm() {}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public int getInip() {
		return inip;
	}

	public void setInip(int inip) {
		this.inip = inip;
	}

	public int getEndp() {
		return endp;
	}

	public void setEndp(int endp) {
		this.endp = endp;
	}

	public String getTagStack() {
		return tagStack;
	}

	public void setTagStack(String tagStack) {
		this.tagStack = tagStack;
	}

	public String getConfidence() {
		return confidence;
	}

	public void setConfidence(String confidence) {
		this.confidence = confidence;
	}

	public String getScoreTag() {
		return scoreTag;
	}

	public void setScoreTag(String scoreTag) {
		this.scoreTag = scoreTag;
	}

	public List<SentimentedConcept> getSentimentedConcepts() {
		return sentimentedConcepts;
	}

	public void setSentimentedConcepts(List<SentimentedConcept> sentimentedConcepts) {
		this.sentimentedConcepts = sentimentedConcepts;
	}

	public List<SentimentedEntity> getSentimentedEntities() {
		return sentimentedEntities;
	}

	public void setSentimentedEntities(List<SentimentedEntity> sentimentedEntities) {
		this.sentimentedEntities = sentimentedEntities;
	}
}
