package com.meaningcloud.domain.sentiment;

import com.fasterxml.jackson.annotation.JsonProperty;

public class SentimentedConcept {

	private String form;
	private String variant;
	private int inip;
	private int endp;
	private String type;
	@JsonProperty("score_tag")
	private String scoreTag;
	
	public SentimentedConcept() {}

	public String getForm() {
		return form;
	}

	public void setForm(String form) {
		this.form = form;
	}

	public String getVariant() {
		return variant;
	}

	public void setVariant(String variant) {
		this.variant = variant;
	}

	public int getInip() {
		return inip;
	}

	public void setInip(int inip) {
		this.inip = inip;
	}

	public int getEndp() {
		return endp;
	}

	public void setEndp(int endp) {
		this.endp = endp;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getScoreTag() {
		return scoreTag;
	}

	public void setScoreTag(String scoreTag) {
		this.scoreTag = scoreTag;
	}
}
