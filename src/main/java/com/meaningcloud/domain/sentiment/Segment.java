package com.meaningcloud.domain.sentiment;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Segment {

	private String text;
	@JsonProperty("segment_type")
	private String segmentType;
	private int inip;
	private int endp;
	private int confidence;
	@JsonProperty("score_tag")
	private String scoreTag;
	private String agreement;
	@JsonProperty("polarity_term_list")
	private List<PolarityTerm> polarityTerms;
	@JsonProperty("segment_list")
	private List<Segment> segments;
	@JsonProperty("sentimented_concept_list")
	private List<SentimentedConcept> sentimentedConcepts;
	@JsonProperty("sentimented_entity_list")
	private List<SentimentedEntity> sentimentedEntities;
	
	public Segment() {}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getSegmentType() {
		return segmentType;
	}

	public void setSegmentType(String segmentType) {
		this.segmentType = segmentType;
	}

	public int getInip() {
		return inip;
	}

	public void setInip(int inip) {
		this.inip = inip;
	}

	public int getEndp() {
		return endp;
	}

	public void setEndp(int endp) {
		this.endp = endp;
	}

	public int getConfidence() {
		return confidence;
	}

	public void setConfidence(int confidence) {
		this.confidence = confidence;
	}

	public String getScoreTag() {
		return scoreTag;
	}

	public void setScoreTag(String scoreTag) {
		this.scoreTag = scoreTag;
	}

	public String getAgreement() {
		return agreement;
	}

	public void setAgreement(String agreement) {
		this.agreement = agreement;
	}

	public List<PolarityTerm> getPolarityTerms() {
		return polarityTerms;
	}

	public void setPolarityTerms(List<PolarityTerm> polarityTerms) {
		this.polarityTerms = polarityTerms;
	}

	public List<Segment> getSegments() {
		return segments;
	}

	public void setSegments(List<Segment> segments) {
		this.segments = segments;
	}

	public List<SentimentedConcept> getSentimentedConcepts() {
		return sentimentedConcepts;
	}

	public void setSentimentedConcepts(List<SentimentedConcept> sentimentedConcepts) {
		this.sentimentedConcepts = sentimentedConcepts;
	}

	public List<SentimentedEntity> getSentimentedEntities() {
		return sentimentedEntities;
	}

	public void setSentimentedEntities(List<SentimentedEntity> sentimentedEntities) {
		this.sentimentedEntities = sentimentedEntities;
	}
}
