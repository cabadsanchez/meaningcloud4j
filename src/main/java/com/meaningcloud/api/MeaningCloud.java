package com.meaningcloud.api;

import java.io.IOException;

import com.meaningcloud.api.template.SentimentTemplate;
import com.meaningcloud.api.template.TopicsTemplate;

public class MeaningCloud {
	
	public static SentimentTemplate sentiment(String key) throws IOException {
		return new SentimentTemplate(key);
	}
	
	public static TopicsTemplate topics(String key) throws IOException {
		return new TopicsTemplate(key);
	}
	
	
}
