package com.meaningcloud.api.template;

import java.io.IOException;
import java.util.Properties;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mashape.unirest.http.HttpMethod;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.exceptions.UnirestException;
import com.meaningcloud.request.template.SentimentRequest;
import com.meaningcloud.response.Sentiment;
import com.meaningcloud.response.errors.MeaningCloudApiException;
import com.meaningcloud.response.errors.MeaningCloudException;

public class SentimentTemplate {

	private String sentimentEndpoint;
	private Properties prop;
	private SentimentRequest request;
	private ObjectMapper mapper;
	
	public SentimentTemplate(String key) throws IOException {
		prop = new Properties();
		prop.load(SentimentTemplate.class.getClassLoader().getResourceAsStream("meaningcloud.properties"));
		mapper = new ObjectMapper();
		
		sentimentEndpoint = prop.getProperty("meaningcloud.api") + prop.getProperty("meaningcloud.sentiment");
			
		request = (SentimentRequest) new SentimentRequest(HttpMethod.POST, sentimentEndpoint).licenseKey(key);
		System.setProperty("jsse.enableSNIExtension", "false");
	}
	
	public SentimentTemplate outputFormat(String of) {
		request.outputFormat(of);
		return this;
	}
	
	public SentimentTemplate verbose(String verbose) {
		request.verbose(verbose);
		return this;
	}
	
	public SentimentTemplate text(String txt) {
		request.text(txt);
		return this;
	}
	
	public SentimentTemplate textFormat(String txtf) {
		request.textFormat(txtf);
		return this;
	}
	
	public SentimentTemplate model(String model) {
		request.model(model);
		return this;
	}
	
	public SentimentTemplate treeView(String tv) {
		request.treeView(tv);
		return this;
	}
	
	public SentimentTemplate expandGlobalPolarity(String egp) {
		request.expandGlobalPolarity(egp);
		return this;
	}
	
	public SentimentTemplate topicTypes(String tt) {
		request.topicTypes(tt);
		return this;
	}
	
	public SentimentTemplate relaxedTipography(String rt) {
		request.relaxedTipography(rt);
		return this;
	}
	
	public SentimentTemplate unknownWords(String uw) {
		request.unknownWords(uw);
		return this;
	}
	
	public SentimentTemplate disambiguationLevel(String dm) {
		request.disambiguationLevel(dm);
		return this;
	}
	
	public SentimentTemplate semanticDisambiguationGrouping(String sdg) {
		request.semanticDisambiguationGrouping(sdg);
		return this;
	}
	
	public SentimentTemplate disambiguationContext(String cont) {
		request.disambiguationContext(cont);
		return this;
	}
	
	public SentimentTemplate showSubtopics(String st) {
		request.showSubtopics(st);
		return this;
	}
	
	public SentimentTemplate caseSensitiveConcepts(String cs) {
		request.caseSensitiveConcepts(cs);
		return this;
	}
	
	public SentimentTemplate timeContext(String timeref) {
		request.timeContext(timeref);
		return this;
	}
	
	public SentimentTemplate userDictionary(String ud) {
		request.userDictionary(ud);
		return this;
	}
	
	public Sentiment analyze() throws UnirestException, JsonParseException, JsonMappingException, IOException, MeaningCloudException, MeaningCloudApiException {
		HttpResponse<JsonNode> response = request.build().asJson();
		Sentiment result = mapper.readValue(response.getRawBody(), Sentiment.class);
		
		if(response.getStatus() != 200)
			throw new MeaningCloudException(response.getStatus(), response.getStatusText());
		else if(result.getStatus().getCode() != 0)
			throw new MeaningCloudApiException(result.getStatus().getCode(), result.getStatus().getMsg());
		
		return result;
	}
}
