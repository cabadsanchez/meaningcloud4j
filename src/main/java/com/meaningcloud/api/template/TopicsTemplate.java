package com.meaningcloud.api.template;

import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.Future;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mashape.unirest.http.HttpMethod;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.async.Callback;
import com.mashape.unirest.http.exceptions.UnirestException;
import com.meaningcloud.request.template.TopicsRequest;
import com.meaningcloud.response.Topics;
import com.meaningcloud.response.errors.MeaningCloudApiException;
import com.meaningcloud.response.errors.MeaningCloudException;

public class TopicsTemplate {

	private String topicsEndpoint;
	private Properties prop;
	private TopicsRequest request;
	private ObjectMapper mapper;
	
	public TopicsTemplate(String key) throws IOException {
		prop = new Properties();
		prop.load(TopicsTemplate.class.getClassLoader().getResourceAsStream("meaningcloud.properties"));
		mapper = new ObjectMapper();
		
		topicsEndpoint = prop.getProperty("meaningcloud.api") + prop.getProperty("meaningcloud.topics");
				
		request = (TopicsRequest) new TopicsRequest(HttpMethod.POST, topicsEndpoint).licenseKey(key);
		System.setProperty("jsse.enableSNIExtension", "false");
	}
	
	public TopicsTemplate outputFormat(String of) {
		request.outputFormat(of);
		return this;
	}
	
	public TopicsTemplate language(String lang) {
		request.language(lang);
		return this;
	}
	
	public TopicsTemplate text(String txt) {
		request.text(txt);
		return this;
	}
	
	public TopicsTemplate textFormat(String txtf) {
		request.textFormat(txtf);
		return this;
	}
	
	public TopicsTemplate url(String url) {
		request.url(url);
		return this;
	}
	
	public TopicsTemplate document(String doc) {
		request.document(doc);
		return this;
	}
	
	public TopicsTemplate topicTypes(String tt) {
		request.topicTypes(tt);
		return this;
	}
	
	public TopicsTemplate unknownWords(String uw) {
		request.unknownWords(uw);
		return this;
	}
	
	public TopicsTemplate relaxedTipography(String rt) {
		request.relaxedTipography(rt);
		return this;
	}
	
	public TopicsTemplate dictionary(String dic) {
		request.dictionary(dic);
		return this;
	}
	
	public TopicsTemplate userDictionary(String ud) {
		request.userDictionary(ud);
		return this;
	}
	
	public TopicsTemplate showSubtopics(String st) {
		request.showSubtopics(st);
		return this;
	}
	
	public Topics analyze() throws UnirestException, JsonParseException, JsonMappingException, IOException, MeaningCloudException, MeaningCloudApiException {
		HttpResponse<JsonNode> response = request.build().asJson();
		Topics result = mapper.readValue(response.getRawBody(), Topics.class);
		
		if(response.getStatus() != 200)
			throw new MeaningCloudException(response.getStatus(), response.getStatusText());
		else if(result.getStatus().getCode() != 0)
			throw new MeaningCloudApiException(result.getStatus().getCode(), result.getStatus().getMsg());
		
		return result;
	}
}
