package com.meaningcloud.examples;

import java.io.IOException;

import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import com.meaningcloud.api.MeaningCloud;
import com.meaningcloud.response.Sentiment;
import com.meaningcloud.response.Topics;
import com.meaningcloud.response.errors.MeaningCloudApiException;
import com.meaningcloud.response.errors.MeaningCloudException;

public class Example {

	public static void main(String[] args) throws IOException, UnirestException, MeaningCloudException, MeaningCloudApiException {
		Topics topics = MeaningCloud.topics("<<Your license key>>")
				.language("es")
				.text("Prueba de funcionamiento de meaningcloud4j.")
				.topicTypes("a")
				.analyze();
		
		Sentiment sentiment = MeaningCloud.sentiment("<<Your license key>>")
				.text("Prueba de funcionamiento de meaningcloud4j.")
				.model("general_es")
				.analyze();
		
		// Do whatever you want with the Topics and Sentiment objects
		
		System.out.println(topics);
		System.out.println(sentiment);
		Unirest.shutdown();
	}
}
